#!/usr/bin/env node

const Battery = require('linux-battery');
const toDecimal = require('to-decimal');

Battery().then(battery => {
	batt = (toDecimal(parseFloat(battery[0].percentage))*100).toFixed(0);
	if (batt < 90 && batt >51) 
		console.log('',batt,'%');
	else if (batt < 50 && batt > 21) 
		console.log('',batt,'%');
	else if (batt < 20) 
		console.log('',batt,'%');
	else 
		console.log('',batt, '%');
});
